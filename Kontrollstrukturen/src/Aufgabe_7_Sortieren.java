import java.util.Arrays;
import java.util.Scanner;

public class Aufgabe_7_Sortieren {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Zu sortierender String: ");
		String str = tastatur.nextLine();
		char charArray[] = str.toCharArray();
		Arrays.sort(charArray);
		System.out.println(new String(charArray));
		
	    tastatur.close();
	}
}
