import java.util.Scanner;

public class Aufgabe_8_Schaltjahr {
	public static void main(String[] args) {
        int jahr;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Geben sie das zu prüfende Jahr ein: ");
        jahr = tastatur.nextInt();
        if(jahr % 4 == 0){
            if(jahr % 100 == 0){
                if(jahr % 400 == 0){
                    System.out.println("Das Jahr ist ein Schaltjahr.");
                }
                else{
                    System.out.println("Das Jahr ist kein Schaltjahr.");
                }
            }
            else{
                System.out.println("Das Jahr ist ein Schaltjahr.");
            }
        }
		
	    tastatur.close();
	}
}