
public class Aufgabe_3 {

	public static void ausgabe_float (int first, double second) {
		System.out.printf("%+-12d|%10.2f\n", first, second);
	}
	public static void main(String[] args) {
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");
		System.out.printf("-----------------------\n");
		ausgabe_float(-20, -28.8889);
		ausgabe_float(-10, -23.3333);
		ausgabe_float(0, -17.7778);
		ausgabe_float(20, -6.6667);
		ausgabe_float(30, -1.1111);

	}

}
