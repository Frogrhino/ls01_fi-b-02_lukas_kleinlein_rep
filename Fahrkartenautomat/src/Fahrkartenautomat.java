import java.text.DecimalFormat;
import java.util.Scanner;
import java.lang.Math;

public class Fahrkartenautomat {
    public static void main(String[] args)
    {
        double zuZahlenderBetrag;
        double rückgabebetrag;
        char exit = 'n';

        Scanner tastatur = new Scanner(System.in);

        while(exit != 'y' && exit != 'Y'){
            zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
            rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
            fahrkartenAusgeben();
            System.out.println("\n\n");
            rückgeldAusgeben(rückgabebetrag);
            System.out.print("\nDen Fahrkartenautomaten beenden?(Y/N): ");
            exit = tastatur.next().charAt(0);
        }

    }
    public static double fahrkartenbestellungErfassen(Scanner tastatur) {
        double zuZahlenderBetrag = 0.0;
        int ticket;
        boolean auswahl = false;
        double[] preis_array = {
                2.90,
                3.30,
                3.60,
                1.90,
                8.60,
                9.00,
                9.60,
                23.50,
                24.30,
                24.90
        };
        String[] bezeichnung_array = {
                "Einzelfahrschein Berlin AB",
                "Einzelfahrschein Berlin BC",
                "Einzelfahrschein Berlin ABC",
                "Kurzstrecke",
                "Tageskarte Berlin AB",
                "Tageskarte Berlin BC",
                "Tageskarte Berlin ABC",
                "Kleingruppen-Tageskarte Berlin AB",
                "Kleingruppen-Tageskarte Berlin BC",
                "kleingruppen-Tageskarte Berlin ABC"
        };

        while(auswahl == false){
            System.out.print("\nFahrkartenbestellvorgang:\n=========================");
            System.out.print("\nWählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
            for(int anzahl_fahrkarten = 0; preis_array.length > anzahl_fahrkarten; anzahl_fahrkarten++){
                System.out.printf("\nFür die Fahrkarte %s (%.2f Euro) drücken sie die %d", bezeichnung_array[anzahl_fahrkarten], preis_array[anzahl_fahrkarten], anzahl_fahrkarten+1);
            }
            System.out.print("\n\nIhre Wahl: ");
            ticket = tastatur.nextInt();
            if(ticket < preis_array.length +1 && ticket > 0) {
                zuZahlenderBetrag = preis_array[ticket - 1];
                auswahl = true;
            }
            else{
                System.out.println("Ihre Auswahl befindet sich nicht im Bereich der Möglichkeiten.");
            }
        }
        System.out.print("Anzahl der Tickets: ");
        zuZahlenderBetrag = tastatur.nextInt() * zuZahlenderBetrag;

        return zuZahlenderBetrag;
    }

    public static double fahrkartenBezahlen(double betrag, Scanner tastatur) {
        double zuZahlenderBetrag = betrag;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag = 0.0;

        DecimalFormat f = new DecimalFormat("#0.00");


        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag){
            System.out.println("Noch zu zahlen: " + f.format((zuZahlenderBetrag - eingezahlterGesamtbetrag)));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }

        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rückgabebetrag;
    }

    public static void fahrkartenAusgeben() {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++){
            System.out.print("=");
            try {
                Thread.sleep(250);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void rückgeldAusgeben(double rückgabebetrag) {
        DecimalFormat f = new DecimalFormat("#0.00");

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        if(rückgabebetrag > 0.0)
        {
            System.out.println("Der Rückgabebetrag in Höhe von " + f.format(rückgabebetrag) + " EURO");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                //System.out.println(rückgabebetrag);
                rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                //System.out.println(rückgabebetrag);
                rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                //System.out.println(rückgabebetrag);
                rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                //System.out.println(rückgabebetrag);
                rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                //System.out.println(rückgabebetrag);
                rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                //System.out.println(rückgabebetrag);

                if(rückgabebetrag > 0.051) {
                    rückgabebetrag = Math.round(rückgabebetrag*10.0)/10.0;
                }
                else {
                    rückgabebetrag = Math.round(rückgabebetrag*1.0)/1.0;
                }
                //System.out.println(rückgabebetrag);
                rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
}